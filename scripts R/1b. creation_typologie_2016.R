# Copyright (C) <2021>. Logiciel élaboré par l'État, via la DREES.

# Nom de l'auteur : <Nathalie Missègue>, DREES.

# Ce programme informatique a été développé par la DREES. Il permet d'actualiser (en millésime communal au 01.01.2016) des tableaux de la typologie "ruralité et lien à la ville" de la DREES publiés dans le Dossier de la DREES <numéro 63>, intitulé « <Appréhender les territoires ruraux dans les études de la DREES> », <Juillet 2020>.

# Ce programme a été exécuté avec la version <4.0.5> de R et les packages <data.table V1.14.0, dplyr V1.0.6, tidyr V1.1.3, sf V0.9-8>

# Les tableaux de l'article peuvent être consultés sur le site de la DREES : <https://drees.solidarites-sante.gouv.fr/sites/default/files/2020-10/dd63.pdf>

# Ce programme utilise les données de l'Insee <Zonage en aire urbaine - ZAU - Grille de densité > et de l'IGN <IGN GEOFLA®>, dans leur version  <ZAU2010, Grille de densité 2016, IGN GEOFLA®>.

# Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr

# Ce logiciel est régi par la licence "GNU General Public License" GPL-3.0. # https://spdx.org/licenses/GPL-3.0.html#licenseText

# À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence GPL-3.0, et que vous en avez accepté les termes.

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.

# Ce code permet de produire la typologie "ruralité et lien à la ville" de la DREES pour 2016, en géographie communale au 01.01.2016

################### packages utilisés ##############################
library(data.table)
library(dplyr)
library(tidyr)
library(sf)

#### Fichiers Insee :(annuels) à télécharger ici :  https://www.insee.fr/fr/information/2028028
# On dé-zip, on ne charge pas les 1ères lignes "d'habillage" du fichier excel 
# On selectionne les cellules qui nous intéressent

# données Insee avec le ZAU2010 et AU2010 pour repérer villes-centres et banlieues
i=16
fich_zip=paste0("donnees entree/table-appartenance-geo-communes-",i,".zip")
fich_exel=paste0("donnees entree/table-appartenance-geo-communes-",i,".xls")
file_url <- "https://www.insee.fr/fr/statistiques/fichier/2028028/table-appartenance-geo-communes-16.zip"
curl::curl_download(url = file_url, destfile = fich_zip)
utils::unzip(fich_zip, exdir = "./donnees entree")

zonage <- readxl::read_excel(
  path = fich_exel,
  sheet = 1,
  range =  "A6:P35891")
zonage <- zonage %>% rename(INSEE_COM=CODGEO) %>%
  select(INSEE_COM,REG,AU2010,CATAEU2010) %>% 
  filter(INSEE_COM !="13055" & INSEE_COM !="69123" & INSEE_COM !="75056" 
         & REG !="06" & REG !="04" & REG !="03" & REG !="02" & REG !="01")                              

# les arrondissements
arond <-  readxl::read_excel(
  path = fich_exel,
  sheet = 2,
  range =  "A6:Q51")
arond <- arond %>% rename(INSEE_COM=CODGEO) %>%
  select(INSEE_COM,REG,AU2010,CATAEU2010)

zonage <- zonage %>% union(arond)

#### La grille de densité
gri=paste0("donnees entree/grille_densite_20",i,".csv")
grille <- fread(gri, sep=";", na.strings="NA", colClasses = 'character', fill=TRUE, data.table = FALSE)
# Paris, Lyon et marseille ne sont pas découpés en arrondissement dans ces données sur la grille de densité
# On supprime les villes en question et on ajoutera les arrondissements en leur affectant à chacun une densité = 1 (puisque ces communes ont une densité = 1)
# On enlève les DOM, absents des autres sources de données

# On vérifie au préalable les noms des colonnes, ils peuvent changer selon le millésime du fichier livré par l'Insee ou l'encodage du fichier
names(grille)

grille <- grille %>% rename(INSEE_COM=`Code commune`,densite=`Typo degré de densité`) %>% 
  filter(INSEE_COM !="13055" & INSEE_COM !="69123" & INSEE_COM !="75056") %>%
  mutate(rg=stringr::str_sub(INSEE_COM,1,2)) %>% 
  filter(stringr::str_detect(rg,"97",negate=TRUE)) %>%
  select(INSEE_COM,densite)
# On recupère les arrondissements et on leur associe la densité=1 (dense)
# Ce fichier est le même pour tous les millésimes de géographie communale 
typo_arr <- read.table(paste0("donnees entree/typo_arrondissements.txt"),header=TRUE,sep="\t", colClasses=c("character","character"))
typo_arr <- typo_arr %>% rename(INSEE_COM=CODGEO)
grille <- grille %>% union(typo_arr)

zonage <- zonage %>% left_join(.,grille,by="INSEE_COM")   

# On utilise les données IGN pour avoir la population, c'est plus simple : sur insee.fr, on a des fichiers par départements pour la population au 01.01.2016 (population légale 2014),
# c'est trop lourd à mobiliser
# Il n'y a pas REG=02 (Martinique) ni REG=04 (La Réunion)
carte <- paste0("donnees entree/Carte20",i,"/")
couche_comm <- st_read(paste0(carte,"COMMUNE.shp"),stringsAsFactors = FALSE) %>% 
  st_set_geometry(NULL) %>%
  select(INSEE_COM,POPULATION)

zonage <- zonage %>% merge(couche_comm, by="INSEE_COM", all.x = TRUE)

# POUR DISTINGUER VILLES-CENTRES ET BANLIEUES DES grands POLES (CATAEU2010 = 111)
# Référence : https://www.insee.fr/fr/statistiques/1283639
# Lorsqu’un grand pôle urbain est constitué de plusieurs
# communes, les communes qui le composent sont soit ville-centre, soit banlieue.
# Si une commune représente plus de 50 % de la population du pôle, elle est la seule
# ville-centre. Sinon, toutes les communes qui ont une population supérieure à 50 %
# de celle de la commune la plus peuplée,ainsi que cette dernière, sont villescentres.
# Les communes urbaines qui ne sont pas villes-centres constituent la banlieue du pôle.

poles <- zonage %>% filter(CATAEU2010 == "111")
# En 2016 : 3 236 grands pôles urbains
# On somme les populations de chaque grand pôle urbain  
popAU <- poles %>% group_by(AU2010) %>% summarise(popAU = sum(POPULATION))
poles <- full_join(poles,popAU,by="AU2010")
# On réordonne les lignes selon la valeur de la population par odre décroissant, pour chaque grand pôle
# et on calcule la part de la population de chaque commune / population totale du pôle
poles <- poles %>% group_by(AU2010) %>% arrange(desc(POPULATION),.by_group = TRUE) %>% 
  mutate(part = (POPULATION / popAU)*100)
# Seuil de sélection des villes centres : soit commune qui a >50% de la population du pôle
# sinon, toutes les communes avec > 50% de la commune la plus peuplée + la commune la plus peuplée
seuil <- poles %>% group_by(AU2010) %>% top_n(1,POPULATION) %>% 
  mutate(POPULATION_first = 0.5*POPULATION) %>% select(AU2010,POPULATION_first)
poles <- full_join(poles,seuil,by="AU2010")
# Paris, Marseille et Lyon : les communes de Paris, Marseille et Lyon sont villes-centres,
# Pour les autres, on prends toutes les communes dont la population est > 50% de la population totale du pôle 

#### Distinction villes-centres/banlieues
poles <- poles %>% mutate(ville=stringr::str_sub(INSEE_COM,1,3)) %>%
  mutate(
    ville_centre = case_when(
      AU2010 =="001" & ville=="751" ~ "ville-centre",
      AU2010 =="002" & ville=="693" ~ "ville-centre",
      AU2010 =="003" & ville=="132" ~ "ville-centre",
      POPULATION > POPULATION_first ~ "ville-centre",
      part > 50 ~ "ville-centre",
      TRUE ~ "banlieue"
    )) %>% ungroup() %>% select(INSEE_COM,ville_centre)

zonage <- zonage %>% 
  left_join(poles,by="INSEE_COM") %>% 
  mutate(
    type_pole = case_when(
      is.na(ville_centre) ~ "autre", 
      ville_centre=="ville-centre" ~ "ville-centre",
      ville_centre=="banlieue" ~ "banlieue",
      TRUE  ~  ""
    )) %>%
  select(-ville_centre)

#### Création typologie des communes : niveau 3, 2 et 1
zonage <- zonage %>% 
  mutate(ruralite_villes_niv3 = case_when(
    AU2010=="001" & type_pole=="ville-centre" ~ "111. Villes-centres du pôle urbain de Paris",
    AU2010=="001" & type_pole=="banlieue" ~ "112. Banlieues du pôle urbain de Paris",
    AU2010=="001" & (densite=="3" | densite=="4") ~ "31. Territoires ruraux des grandes aires",
    AU2010=="001" & (densite=="1" | densite=="2") ~ "113. Communes non rurales de l'aire urbaine de Paris",
    type_pole=="ville-centre" ~ "121. Villes-centres des autres grands pôles urbains",
    type_pole=="banlieue" ~ "122. Banlieues des autres grands pôles urbains",
    (CATAEU2010=="112" | CATAEU2010=="120") & (densite=="3" | densite=="4") ~ "31. Territoires ruraux des grandes aires",
    (CATAEU2010=="112" | CATAEU2010=="120") & (densite=="1" | densite=="2") ~ "123. Communes non rurales des grandes aires urbaines",
    CATAEU2010=="111" ~ "123. Communes non rurales des grandes aires urbaines",
    (CATAEU2010=="212" | CATAEU2010=="222" | CATAEU2010=="300") & (densite=="3" | densite=="4") ~ "32. Territoires ruraux des moyennes et petites aires",
    (CATAEU2010=="212" | CATAEU2010=="222" | CATAEU2010=="300" | CATAEU2010=="400") & (densite=="1" | densite=="2") ~ "2. Moyennes, petites aires et multipol. hors ruraux",
    (CATAEU2010=="211" | CATAEU2010=="221") ~ "2. Moyennes, petites aires et multipol. hors ruraux",
    CATAEU2010=="400" & (densite=="3" | densite=="4") ~ "33. Territoires ruraux isolés",
    TRUE ~ "autres"))

table(zonage$ruralite_villes_niv3)
zonage <- zonage %>% mutate(ruralite_villes_niv2 = case_when(
  ruralite_villes_niv3=="31. Territoires ruraux des grandes aires" ~ "31. Territoires ruraux des grandes aires",  
  ruralite_villes_niv3=="32. Territoires ruraux des moyennes et petites aires" ~ "32. Territoires ruraux des moyennes et petites aires",
  ruralite_villes_niv3=="33. Territoires ruraux isolés" ~ "33. Territoires ruraux isolés",  
  AU2010=="001" & ruralite_villes_niv3 !="31. Territoires ruraux des grandes aires" ~ "11. Aire urbaine de Paris, hors terr. ruraux",
  (CATAEU2010=="111" | CATAEU2010=="112" | CATAEU2010=="120") & ruralite_villes_niv3 !="31. Territoires ruraux des grandes aires" ~ "12. Grandes aires urbaines, hors Paris et terr. ruraux",
  ruralite_villes_niv3=="2. Moyennes, petites aires et multipol. hors ruraux" ~ "2. Moyennes, petites aires et multipol. hors ruraux",
  TRUE ~ "autres"))

table(zonage$ruralite_villes_niv2)

zonage <- zonage %>% mutate(ruralite_villes_niv1 = case_when(
  (ruralite_villes_niv3=="31. Territoires ruraux des grandes aires" | 
     ruralite_villes_niv3=="32. Territoires ruraux des moyennes et petites aires" | 
     ruralite_villes_niv3=="33. Territoires ruraux isolés") ~  "3. Territoires ruraux des grandes, moyennes, petites aires et isolés",
  (CATAEU2010=="111" | CATAEU2010=="112" | CATAEU2010=="120") 
  & ruralite_villes_niv3 !="31. Territoires ruraux des grandes aires" ~ "1. Grandes aires urbaines, hors terr. ruraux",
  ruralite_villes_niv3=="2. Moyennes, petites aires et multipol. hors ruraux" ~ "2. Moyennes, petites aires et multipol. hors ruraux",
  TRUE ~ "autres"))

table(zonage$ruralite_villes_niv1)

zonage_drees_2016 <- zonage %>% select(INSEE_COM,ruralite_villes_niv3,ruralite_villes_niv2,ruralite_villes_niv1)
save(zonage_drees_2016,file="donnees sortie/zonage_drees_2016.Rdata")
write.csv(zonage_drees_2016,file="donnees sortie/zonage_drees_2016.csv",row.names = F)

rm(arond,couche_comm,gri,grille,poles,popAU,seuil,typo_arr,zonage)

# On supprime les fichiers téléchargés au début et le fichier excel correspondant
file.remove(fich_zip)
file.remove(fich_exel)
gc()
