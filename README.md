# Dossier de la DREES n°63, Appréhender les territoires ruraux dans les études de la DREES, Juillet 2020

Ce dossier fournit tout d'abord les programmes permettant de produire la typologie développée par la DREES nommée "ruralité et liens à la ville" (millésime au 1er janvier 2015 de la géographie communale) et de l'actualiser (en millésimes 2016 à 2019). Cette typologie (millésime 2015 de la géographie communale) est publiée dans le Dossier de la DREES n°63, juillet 2020 : "<Appréhender les territoires ruraux dans les études de la DREES>". 

Le dossier est organisé de la manière suivante : 

- il comporte 2 dossiers, 'donnees sortie' et 'scripts R'. 

- Dans 'scripts R' figurent les scripts '1a. creation_typologie' à '1.e  creation_typologie', dont les noms sont sufixés par '_ANNEE', pour chacun des millésimes de la géographie communale (ANNEE = 2015 à 2019), ainsi que le script '2. cartes_typologie.R' qui permet de produire les cartes 5 et 6 publiées dans le Dossier de la DREES, ainsi qu'une carte (non publiée dans ce dossier) qui dessine la typologie à son niveau le plus regroupé, en 3 catégories (typologie niveau 1, voir page 34 du Dossier de la DREES n°63). Dans les scripts permettant de créer la typologie, les données sur le zonage en aires urbaines sont téléchargées directement sur le site de l'Insee (les adresses des pages html figurent dans chacun des scripts R).

- Dans 'donnees sorties' seront stockés les fichiers produits de la typologie (un au format RData et un au format .csv).
A signaler qu'à compter de 2020 (millésime communal au 1er janvier 2020) le zonage en aires urbaines 2010 (ZAU 2010) utilisé pour créer la typologie "ruralité et liens à la ville" de la DREES est remplacé par le zonage en aires d’attraction des villes : https://www.insee.fr/fr/information/4803954. Ce nouveau zonage de l'Insee ne permet pas dans l'immédiat de mettre à jour automatiquement la typologie de la DREES. La DREES remercie l'Insee pour la fourniture de données ayant permis d'élaborer la typologie de la DREES.

- ****Attention** : un troisième dossier, 'donnees entree' doit être récupéré ici : https://data.drees.solidarites-sante.gouv.fr/explore/dataset/typologie-des-territoires-ruraux-et-urbains-/information/** (ceci afin de ne pas stocker sur Gitlab des fichiers volumineux). Ce dossier contient les données de l'IGN (année 2015) permettant de récupérer aisément la population communale qui est utilisée pour contruire la typologie. Chacun des scripts R des années 2016 et 2017 peut être adapté par l'utilisateur afin de télécharger directement depuis le site internet de l'IGN, les fichiers 'COMMUNE.shp' utilisés. Les données 2016 (de même que 2015) de l'IGN sont téléchargeables ici : https://geoservices.ign.fr/geofla, rubrique 'Geofla® Communes' (sous-rubriques 'Geofla® Communes édition 2016 France Métropolitaine' et 'Geofla® Communes édition 2015 France Métropolitaine') et pour l'année 2017 sur la page https://geoservices.ign.fr/adminexpress, rubrique 'ADMIN-EXPRESS-COG', sous-rubrique 'ADMIN-EXPRESS-COG édition 2017 par territoire'. Les années suivantes (2018 et 2019) le fichier de population légale est téléchargé directement sur le site internet de l'Insee (la page web à télécharger figure dans chaque script annuel (du dossier 'scripts R'). Figurent aussi dans ce dossier : les grilles de densité (2015 en format RData ; 2016 à 2019 en format .csv ou .ods) et un fichier avec la catégorie de densité (1) affectée à chaque arrondissement de Paris, Lyon et Marseille (cette information étant absente des grilles de densité fournies par l'Insee).

Lien vers la publication  :  https://drees.solidarites-sante.gouv.fr/sites/default/files/2020-10/dd63.pdf

Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : zonage en aire urbaine - ZAU2010, grilles de densité de l'Insee et populations légales (2018 et 2019), données de l'IGN (2015 à 2017). Traitements : DREES.

Les programmes ont été exécutés pour la dernière fois le 5 octobre 2021 avec le logiciel R version <4.0.5>.
